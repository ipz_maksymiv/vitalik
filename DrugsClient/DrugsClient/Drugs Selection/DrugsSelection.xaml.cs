﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace DrugsClient
{
    /// <summary>
    /// Interaction logic for DrugsSelection.xaml
    /// </summary>
    public partial class DrugsSelection : Window
    {
        private String OrderDrugs;
        private bool AddClicked;
        private bool Exception;
        private Drug[] AvailableDrugs;
        private Int32[] MaxInAvailable;
               
        
        public DrugsSelection()
        {
            InitializeComponent();
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            Uri Icon=new Uri("IconApp.ico",UriKind.RelativeOrAbsolute);
            this.Icon=BitmapFrame.Create(Icon);

            Exception=false;
            ButtonAdd.IsEnabled=false;
            AddClicked=false;
            String result=Connect("127.0.0.1","GetDrugs","");
            if (Exception){
                Exception=false;
                this.Close();
                return;}
            if (result[0]=='D'){
                String[] items=result.Split('~');
                int length=items.Length-1;
                for (int a=1;a<items.Length;a++){
                    if (items[a]==""){
                        length--;}}
                try
                {
                AvailableDrugs=new Drug[length];
                for (int a=0,b=1;a<AvailableDrugs.Length && b<items.Length;a++,b++){
                    if (items[b]==""){
                        a--;
                        continue;}
                    AvailableDrugs[a]=new Drug(items[b].Split('`')[0],Int32.Parse(items[b].Split('`')[1]),Double.Parse(items[b].Split('`')[2]));}
                }

                catch(FormatException exception)
                {
                    String ex=exception.Data.ToString();
                    MessageBox.Show("Bad data in data base","Data base error",MessageBoxButton.OK);
                    this.Close();
                    return;
                }
                
                MaxInAvailable=new Int32[AvailableDrugs.Length];
                for (int a=0;a<MaxInAvailable.Length;a++){
                    MaxInAvailable[a]=AvailableDrugs[a].PQuantity;}

                for (int a=0;a<AvailableDrugs.Length;a++){
                    AvailableDrugs[a].PQuantity=1;}

                if (AvailableDrugs.Length!=0){
                    for (int a=0;a<AvailableDrugs.Length;a++){
                        ListViewDrugs.Items.Add(AvailableDrugs[a]);}}
                else{
                    MessageBox.Show("There are no available drugs in Drugs Shop","No available drugs",MessageBoxButton.OK);
                    this.Close();
                    return;}}
                                    
            else if (result[0]=='E'){
                String[] str=result.Split('~');
                MessageBox.Show(str[1],str[2],MessageBoxButton.OK);
                this.Close();}            
        }

        private void ButtonAddClick(object sender, RoutedEventArgs e)
        {
            String Drugs="";
            int index=0;
            Drug[] SelectedDrugs=new Drug[ListViewDrugs.SelectedItems.Count];
            foreach (Drug drug in ListViewDrugs.SelectedItems){
                SelectedDrugs[index++]=drug;}

            for (int a=0;a<SelectedDrugs.Length;a++){
                Drugs+=SelectedDrugs[a].PName+"`"+SelectedDrugs[a].PQuantity.ToString()+"`"+SelectedDrugs[a].PPrice.ToString()+"\n";}
                        
            String result=Connect("127.0.0.1","AddToOrder",Drugs);
            if (Exception){
                Exception=false;
                return;}
            if (result[0]=='A'){
                AddClicked=true;
                OrderDrugs+=result.Split('~')[1];
                MessageBox.Show("Selected drugs have been added to your order","Drugs to order",MessageBoxButton.OK);
                this.Close();}
            else if (result[0]=='E'){
                String[] str=result.Split('~');
                MessageBox.Show(str[1],str[2],MessageBoxButton.OK);}
        }
                

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
           if (ListViewDrugs.SelectedIndex==-1){
               ButtonAdd.IsEnabled=false;}
           else{
               ButtonAdd.IsEnabled=true;}
        }


        String Connect(String Server,String Action,String Drug)
        {
            String received="";
            TcpClient client=null;
            NetworkStream clientStream=null;

            try
            {
            String str=Action+"~"+Drug;

            Int32 Port=5050;
            client=new TcpClient(Server,Port);

            Byte[] Data=System.Text.Encoding.ASCII.GetBytes(str);
            clientStream = client.GetStream();
               
            clientStream.Write(Data,0,Data.Length);
            
            
            Data=new Byte[65536];

            received=String.Empty;

            Int32 Bytes=clientStream.Read(Data,0,Data.Length);
            received=System.Text.Encoding.ASCII.GetString(Data,0,Bytes);
            }

            catch(SocketException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("Can't connect to server. Socket error","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(IOException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("NetworkStream read or write error","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(ObjectDisposedException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("NetworkStream or TcpClient is closed","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(InvalidOperationException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("Tcp client is not connected","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            finally
            {
                if (clientStream!=null){
                    clientStream.Close();}
                if (client!=null){
                    client.Close();}            
            }
          
            return received;
        }
    
        public String POrderDrugs
        {
            get{return OrderDrugs;}
        }         
       

        public bool PAddClicked
        {
            get
            {
                if (AddClicked==true){
                    AddClicked=false;
                    return true;}
                else{
                    return false;}
            }
        }


        private void TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            char key=(char)KeyInterop.VirtualKeyFromKey(e.Key);
            if (!Char.IsDigit(key) && (e.Key<Key.NumPad0 || e.Key>Key.NumPad9)){
                    e.Handled=true;}                    
        }

        private void TextBoxKeyUp(object sender, KeyEventArgs e)
        {
            TextBox textBox=sender as TextBox;
            if (textBox.Text=="" || textBox.Text=="0"){
                textBox.Text="1";}

            System.Windows.Threading.DispatcherTimer timer=new System.Windows.Threading.DispatcherTimer();
            timer.Interval=new TimeSpan(0,0,1);
            timer.Tick+=new EventHandler(TimerTick);        
            timer.Start();    
        }

        private void TimerTick(object sender, EventArgs e)
        {
            Drug[] AllItems=new Drug[ListViewDrugs.Items.Count];

            int ind=0;
            foreach(Drug drug in ListViewDrugs.Items){
                    AllItems[ind++]=drug;}

            for (int a=0;a<AllItems.Length;a++){
                for (int b=0;b<MaxInAvailable.Length;b++){
                    if (AllItems[a].PName==AvailableDrugs[b].PName){
                        if (AllItems[a].PQuantity>MaxInAvailable[b]){
                            AllItems[a].PQuantity=MaxInAvailable[b];
                            break;}}}}

            String Drugs="";

            for (int a=0;a<AllItems.Length;a++){
                Drugs+=AllItems[a].PName+"`"+AllItems[a].PQuantity.ToString()+"`"+AllItems[a].PPrice.ToString()+"\n";}

            String result=Connect("127.0.0.1","QuantityChanged",Drugs);
            if (Exception){
                Exception=false;
                return;}
            if (result[0]=='Q'){
                String[] NewDrugs=result.Split('~')[1].Split('\n');
               
                for (int a=0;a<AllItems.Length;a++){
                    if (NewDrugs[a]!=""){
                        AllItems[a]=new Drug(NewDrugs[a].Split('`')[0],Int32.Parse(NewDrugs[a].Split('`')[1]),Double.Parse(NewDrugs[a].Split('`')[2]));}}

                ListViewDrugs.Items.Clear();
                ListViewDrugs.Items.Refresh();

                for (int a=0;a<AllItems.Length;a++){
                    ListViewDrugs.Items.Add(AllItems[a]);}

                ListViewDrugs.Items.Refresh();}
           
            System.Windows.Threading.DispatcherTimer timer=sender as System.Windows.Threading.DispatcherTimer;
            timer.Stop();
        }
    }
}
