﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace DrugsClient
{
    /// <summary>
    /// Interaction logic for Order.xaml
    /// </summary>
    public partial class Order : Window
    {
        private PersonalData personalData;
        private String OrderDrugs;
        private bool Confirmed;
        private bool Canceled;
        private String Discount;
        private String Overall;
        private bool Exception;


        public Order()
        {
            InitializeComponent();
        }
        
        String Connect(String Server,String Action,String LastName,String FirstName,String Phone,String Drugs,String Sum)
        {
            String received="";
            TcpClient client=null;
            NetworkStream clientStream=null;

            try
            {
            String str=Action+"~"+LastName+"~"+FirstName+"~"+Phone+"~"+Drugs+"~"+Sum;

            Int32 Port=5050;
            client=new TcpClient(Server,Port);

            Byte[] Data=System.Text.Encoding.ASCII.GetBytes(str);
            clientStream = client.GetStream();
               
            clientStream.Write(Data,0,Data.Length);
            
            
            Data=new Byte[65536];

            received=String.Empty;

            Int32 Bytes=clientStream.Read(Data,0,Data.Length);
            received=System.Text.Encoding.ASCII.GetString(Data,0,Bytes);
            }

            catch(SocketException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("Can't connect to server. Socket error","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(IOException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("NetworkStream read or write error","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(ObjectDisposedException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("NetworkStream or TcpClient is closed","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(InvalidOperationException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("Tcp client is not connected","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            finally
            {
                if (clientStream!=null){
                    clientStream.Close();}
                if (client!=null){
                    client.Close();}            
            }
            
            return received;
        }                     


        public PersonalData PData
        {
            set{personalData=value;}
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            Uri Icon=new Uri("IconApp.ico",UriKind.RelativeOrAbsolute);
            this.Icon=BitmapFrame.Create(Icon);

            Exception=false;
            GroupBoxOrder.IsEnabled=true;
            if (Canceled){
                GroupBoxOrder.IsEnabled=false;
                LabelSum.Content="0.0";
                LabelDiscount.Content="0.0";
                LabelOverall.Content="0.0";
                return;}
    
            Bind(OrderDrugs);

             if (ListViewDrugs.Items.Count==0){
                GroupBoxOrder.IsEnabled=false;
                LabelSum.Content="0.0";
                LabelDiscount.Content="0.0";
                LabelOverall.Content="0.0";
                return;}

            String result=Connect("127.0.0.1","OrderSum","","","",OrderDrugs,"");
            if (Exception){
                Exception=false;
                this.Close();
                return;}
            try
            {
            if (result[0]=='S'){
                GroupBoxOrder.IsEnabled=true;
                String sum=result.Split('~')[1];
                
                LabelSum.Content=sum;
                LabelOverall.Content=sum;
                if (Confirmed){
                    LabelDiscount.Content=Discount;
                    LabelOverall.Content=Overall;
                    ButtonDiscount.Visibility=Visibility.Hidden;
                    GroupBoxOrder.IsEnabled=false;}}
            else if (result[0]=='N'){
                GroupBoxOrder.IsEnabled=false;
                LabelSum.Content="0.0";
                LabelDiscount.Content="0.0";
                LabelOverall.Content="0.0";}
            else if (result[0]=='E'){
                String[] str=result.Split('~');
                MessageBox.Show(str[1],str[2],MessageBoxButton.OK);
                this.Close();}
            }

            catch(FormatException exception)
            {
                    String ex=exception.Data.ToString();
                    MessageBox.Show("Bad data in data base","Data base error",MessageBoxButton.OK);
                    this.Close();
                    return;
            }
        }

        private void ButtonCancel_Click(object sender, RoutedEventArgs e)
        {            
            String result=Connect("127.0.0.1","CancelOrder","","","",OrderDrugs,"");
            if (Exception){
                Exception=false;
                return;}
            if (result[0]=='C'){
                OrderDrugs="";
                Canceled=true;
                Confirmed=false;}
            MessageBox.Show("Order has been canceled","Cancel Order",MessageBoxButton.OK);
            this.Close();

        }

        private void ButtonConfirm_Click(object sender, RoutedEventArgs e)
        {
            String result=Connect("127.0.0.1","UpdateClient",personalData.PLast,personalData.PFirst,personalData.PPhone,"",LabelOverall.Content.ToString());
            if (Exception){
                Exception=false;
                return;}
            if (result[0]=='U'){
                ButtonDiscount.Visibility=Visibility.Hidden;
                MessageBox.Show("Overall sum is "+LabelOverall.Content.ToString()+". Thanks)","Order Confirmed",MessageBoxButton.OK);
                GroupBoxOrder.IsEnabled=false;
                Canceled=false;
                Confirmed=true;} 
            else if (result[0]=='E'){
                String[] str=result.Split('~');
                MessageBox.Show(str[1],str[2],MessageBoxButton.OK);}            
        }


        public String POrderDrugs
        {
            get{return OrderDrugs;}
            set{OrderDrugs+="\n"+value;}
        }

        private void ButtonDiscount_Click(object sender, RoutedEventArgs e)
        {
            String result=Connect("127.0.0.1","GetDiscount",personalData.PLast,personalData.PFirst,personalData.PPhone,"",LabelSum.Content.ToString());
            if (Exception){
                Exception=false;
                return;}
            try
            {
            if (result[0]=='D'){
                String[] str=result.Split('~');
                LabelDiscount.Content=str[1];
                LabelOverall.Content=str[2];
                ButtonDiscount.Visibility=Visibility.Hidden;}
            else if (result[0]=='E'){
                String[] str=result.Split('~');
                MessageBox.Show(str[1],str[2],MessageBoxButton.OK);}
            }

            catch(FormatException exception)
            {
                    String ex=exception.Data.ToString();
                    MessageBox.Show("Bad data in data base","Data base error",MessageBoxButton.OK);
                    this.Close();
                    return;
            }

            catch(IndexOutOfRangeException exception)
            {
                    String ex=exception.Data.ToString();
                    MessageBox.Show("Bad data in data base","Data base error",MessageBoxButton.OK);
                    return;
            }
        }

        public bool PConfirmed
        {
            get{return Confirmed;}
            set{Confirmed=value;}
        }

        public bool PCanceled
        {
            get{return Canceled;}
            set{Canceled=value;}
        }

        public String PDiscount
        {
            get{return LabelDiscount.Content.ToString();}
            set{Discount=value;}
        }

        public String POverall
        {
            get{return LabelOverall.Content.ToString();}
            set{Overall=value;}
        }


        void Bind(String str)
        {
            String[] data=str.Split('\n');
            String[] drug;
                       
            for (int a=0;a<data.Length;a++){
                if (data[a]!=""){
                    drug=data[a].Split('`');
                    ListViewDrugs.Items.Add(new {DDrug=drug[0],NNum=drug[1],PPrice=drug[2]});}}

        }        
    }
}
