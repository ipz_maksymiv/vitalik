﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugsClient
{
    public class PersonalData
    {
        private String LastName;
        private String FirstName;
        private String Phone;

        public PersonalData(String last,String first,String phone)
        {
            LastName=last;
            FirstName=first;
            Phone=phone;
        }

        ~PersonalData(){}

        public String PLast
        {
            get{return LastName;}
        }

        public String PFirst
        {
            get{return FirstName;}
        }

        public String PPhone
        {
            get{return Phone;}
        }
    };
}
