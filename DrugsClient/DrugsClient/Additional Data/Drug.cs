﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugsClient
{
    public class Drug
    {
        private String Name;
        private Int32 Quantity;
        private Double Price;
        private List<String> DrugReviews;
        
        public Drug()
        {
            Name="";
            Quantity=0;
            Price=0.0;            
            DrugReviews=new List<String>();
        }

        public Drug(String name,int quantity,double price)
        {
            Name=name;
            Quantity=quantity;
            Price=price;            
            DrugReviews=new List<String>();
        }

        ~Drug(){}
       
        public String PName
        {
            get{return Name;}
            set{Name=value;}
        }

        public Int32 PQuantity
        {
            get{return Quantity;}
            set{Quantity=value;}
        }

        public double PPrice
        {
            get{return Price;}
            set{Price=value;}
        }

        public List<String> GetReviews()
        {
           return DrugReviews;
        }

        public void SetReview(String review)
        {
            DrugReviews.Add(review);
        }
    };
}
