﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.IO;


namespace DrugsClient
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        private  PersonalData personalData;
        private bool OrderConfirmed;
        private bool OrderCanceled;
        private String Discount;
        private String Overall;
        private String OrderDrugs;
        private bool AddClicked;
        private bool Exception;

        public MainWindow()
        {
            InitializeComponent();
        }

        private void DrugsClick(object sender, RoutedEventArgs e)
        {
            DrugsSelection drugs=new DrugsSelection();
            drugs.ShowDialog();
            AddClicked=drugs.PAddClicked;
            OrderDrugs+=drugs.POrderDrugs;
        }

        private void ReviewsClick(object sender, RoutedEventArgs e)
        {
            Reviews reviews=new Reviews();
            reviews.ShowDialog();
        }

        private void OrderClick(object sender, RoutedEventArgs e)
        {
            if (AddClicked){
                OrderCanceled=false;
                AddClicked=false;}
            Order order=new Order();
            if (OrderDrugs==null){
                OrderDrugs="";}
            order.POrderDrugs=OrderDrugs;
            order.PConfirmed=OrderConfirmed;
            order.PCanceled=OrderCanceled;
            order.PDiscount=Discount;
            order.POverall=Overall;
            order.PData=personalData;
            order.ShowDialog();
            OrderConfirmed=order.PConfirmed;
            if (OrderConfirmed){
                ButtonSelect.IsEnabled=false;}
            OrderCanceled=order.PCanceled;
            if (OrderCanceled){
                OrderDrugs="";}
            Overall=order.POverall;
            Discount=order.PDiscount;
        }

                
        private void TextBoxLastTextChanged(object sender, TextChangedEventArgs e)
        {
            if (!CorrectData()){
                ButtonLogin.IsEnabled=false;}
            else{
                ButtonLogin.IsEnabled=true;}
        }

        private bool CorrectData()
        {
            if (TextBoxLast.Text=="" || TextBoxFirst.Text=="" || TextBoxPhone.Text==""){
                return false;}

            if (TextBoxLast.Text.Contains(" ")){
                TextBoxLast.Foreground=Brushes.Red;
                return false;}
            else{
                TextBoxLast.Foreground=Brushes.Black;}

            if (TextBoxFirst.Text.Contains(" ")){
                TextBoxFirst.Foreground=Brushes.Red;
                 return false;}
            else{
                TextBoxFirst.Foreground=Brushes.Black;}

            if (TextBoxPhone.Text.Contains(" ")){
                TextBoxPhone.Foreground=Brushes.Red; 
                return false;}
            else{
                TextBoxPhone.Foreground=Brushes.Black;}

           return true;
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            Exception=false;
            AddClicked=false;
            OrderDrugs="";
            Discount="0.0";
            Overall="0.0";
            OrderConfirmed=false;
            OrderCanceled=false;
            ButtonLogin.IsEnabled=false;
            GroupBoxOperations.IsEnabled=false;
            Uri Icon=new Uri("IconApp.ico",UriKind.RelativeOrAbsolute);
            this.Icon=BitmapFrame.Create(Icon);
        }

        private void TextBoxKeyDown(object sender, KeyEventArgs e)
        {
            char key=(char)KeyInterop.VirtualKeyFromKey(e.Key);
            TextBox textBox=(TextBox)sender;

			if (key=='\t'){
				FocusNavigationDirection focusDirection = FocusNavigationDirection.Next;
				TraversalRequest request = new TraversalRequest(focusDirection);
				UIElement elementWithFocus = Keyboard.FocusedElement as UIElement;
				if (elementWithFocus != null){
				if (elementWithFocus.MoveFocus(request)){
					e.Handled = true;}}}

            if (textBox==TextBoxPhone){
                if (!Char.IsDigit(key) && (e.Key<Key.NumPad0 || e.Key>Key.NumPad9)){
                    e.Handled=true;}}
            else{
                if (!Char.IsLetter(key) || (e.Key>=Key.NumPad0 && e.Key<=Key.NumPad9)){
                    e.Handled=true;}}
        }

        private void LoginClick(object sender, RoutedEventArgs e)
        {
            personalData=new PersonalData(TextBoxLast.Text,TextBoxFirst.Text,TextBoxPhone.Text);
            String result=Connect("127.0.0.1","IsRegistered",personalData.PLast,personalData.PFirst,personalData.PPhone,"");
            if (Exception){
                Exception=false;
                return;}
            if (result=="Authorized"){
                MessageBox.Show("Client successfully authorized","Authorization successful",MessageBoxButton.OK);
                GroupBoxAuthorization.IsEnabled=false;
                GroupBoxOperations.IsEnabled=true;}
            else if (result[0]=='E'){
                String[] str=result.Split('~');
                MessageBox.Show(str[1],str[2],MessageBoxButton.OK);}
            else{
               if(MessageBox.Show("This client is unregistered. Do you want register?","Client registration",MessageBoxButton.OKCancel)==MessageBoxResult.OK){
                    String register=Connect("127.0.0.1","Register",personalData.PLast,personalData.PFirst,personalData.PPhone,"");
                    if (register=="Registered"){
                        MessageBox.Show("Client successfully registered","Registration successful",MessageBoxButton.OK);
                        GroupBoxAuthorization.IsEnabled=false;
                        GroupBoxOperations.IsEnabled=true;}
                    else{
                        MessageBox.Show("Client registration error. This phone has been registered with other last and first names","Registration failed",MessageBoxButton.OK);}}}              
        }

       
        String Connect(String Server,String Action,String LastName,String FirstName,String Phone,String Drugs)
        {
            String received="";
            TcpClient client=null;
            NetworkStream clientStream=null;

            try
            {
            String str=Action+"~"+LastName+"~"+FirstName+"~"+Phone+"~"+Drugs;

            Int32 Port=5050;
            client=new TcpClient(Server,Port);

            Byte[] Data=System.Text.Encoding.ASCII.GetBytes(str);
            clientStream=client.GetStream();
               
            clientStream.Write(Data,0,Data.Length);
                        
            Data=new Byte[65536];

            Int32 Bytes=clientStream.Read(Data,0,Data.Length);
            received=System.Text.Encoding.ASCII.GetString(Data,0,Bytes);
            }
            
            catch(SocketException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("Can't connect to server. Socket error","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(IOException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("NetworkStream read or write error","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(ObjectDisposedException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("NetworkStream or TcpClient is closed","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(InvalidOperationException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("Tcp client is not connected","Connection failed",MessageBoxButton.OK);                
                Exception=true;
            }

            finally
            {
                if (clientStream!=null){
                    clientStream.Close();}
                if (client!=null){
                    client.Close();}            
            }

            return received;
        }

        private void MainWindowClosing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            if (OrderDrugs!="" && !OrderConfirmed){
                if (MessageBox.Show("You order hasn't been confirmed. If you close this window, your order will have been canceled. Do you want close this window?","Order is not confirmed",MessageBoxButton.YesNo)==MessageBoxResult.Yes){
                    String result=Connect("127.0.0.1","CancelOrder","","","",OrderDrugs);
                    if (Exception){
                        Exception=false;
                        e.Cancel=true;
                        return;}
                    if (result[0]=='C'){
                        MessageBox.Show("Order has been canceled","Cancel Order",MessageBoxButton.OK);}}
                else{
                    e.Cancel=true;}}                
        }                     
    }
}
