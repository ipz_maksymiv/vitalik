﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace DrugsClient
{
    /// <summary>
    /// Interaction logic for Reviews.xaml
    /// </summary>
    public partial class Reviews : Window
    {
        private bool Exception;

        public Reviews()
        {
            InitializeComponent();
        }

        private void Load(object sender, RoutedEventArgs e)
        {
            Uri Icon=new Uri("IconApp.ico",UriKind.RelativeOrAbsolute);
            this.Icon=BitmapFrame.Create(Icon);

            Exception=false;
            ButtonGet.IsEnabled=false;
            ButtonSet.IsEnabled=false;
            ComboBoxAction.SelectedIndex=0;
            ComboBoxAction.IsEnabled=false;
            TextBoxReviews.IsEnabled=false;
            ComboBoxAction.Items.Add("Get Reviev(s)");
            ComboBoxAction.Items.Add("Set Reviev");
            


            String result=Connect("127.0.0.1","GetDrugsReviews","","");
            if (Exception){
                Exception=false;
                this.Close();
                return;}
            if (result[0]=='D'){
               String[] items=result.Split('~');
               for (int a=1;a<items.Length;a++){
                   ListBoxDrugs.Items.Add(items[a]);}}   
            else if (result[0]=='E'){
                String[] str=result.Split('~');
                MessageBox.Show(str[1],str[2],MessageBoxButton.OK);
                this.Close();}
        }

        private void SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            TextBoxReviews.Clear();
            if (ComboBoxAction.SelectedIndex==0 && ComboBoxAction.IsEnabled){
                TextBoxReviews.IsReadOnly=true;
                ButtonGet.IsEnabled=true;
                ButtonSet.IsEnabled=false;}
            else{
                TextBoxReviews.IsReadOnly=false;
                ButtonGet.IsEnabled=false;
                ButtonSet.IsEnabled=false;}
        }

        private void TextChanged(object sender, TextChangedEventArgs e)
        {
            if (TextBoxReviews.Text.Length==0 || ComboBoxAction.SelectedIndex==0){
                ButtonSet.IsEnabled=false;}
            else{
                ButtonSet.IsEnabled=true;}
        }

        private void ButtonGetClick(object sender, RoutedEventArgs e)
        {
            TextBoxReviews.Clear();
            String result=Connect("127.0.0.1","GetReview",ListBoxDrugs.Items[ListBoxDrugs.SelectedIndex].ToString(),"");
            if (Exception){
                Exception=false;
                return;}
            if (result[0]=='G'){
                for (int a=2;a<result.Length;a++){
                    if (result[a]=='~'){
                        TextBoxReviews.Text+="\n\n";
                        continue;}
                    TextBoxReviews.Text+=result[a];}}
            else if (result[0]=='N'){
                MessageBox.Show("There are not review(s) of this drug","No Reviews",MessageBoxButton.OK);}
            else if (result[0]=='E'){
                String[] str=result.Split('~');
                MessageBox.Show(str[1],str[2],MessageBoxButton.OK);}              
        }

        private void ButtonSetClick(object sender, RoutedEventArgs e)
        {
            String result=Connect("127.0.0.1","SetReview",ListBoxDrugs.Items[ListBoxDrugs.SelectedIndex].ToString(),TextBoxReviews.Text);
            if (Exception){
                Exception=false;
                return;}
            if (result[0]=='S'){
                MessageBox.Show("Review has been added","Set Review",MessageBoxButton.OK);
                TextBoxReviews.Clear();}
            else if (result[0]=='E'){
                String[] str=result.Split('~');
                MessageBox.Show(str[1],str[2],MessageBoxButton.OK);}
        }

        private void ListBoxSelection(object sender, SelectionChangedEventArgs e)
        {
            if (ComboBoxAction.SelectedIndex==0){
                TextBoxReviews.IsReadOnly=true;
                ButtonGet.IsEnabled=true;
                ButtonSet.IsEnabled=false;}
            else{
                TextBoxReviews.IsReadOnly=false;
                ButtonGet.IsEnabled=false;}

            TextBoxReviews.Text="";
            ComboBoxAction.IsEnabled=true;
            TextBoxReviews.IsEnabled=true;            
        }


        String Connect(String Server,String Action,String Drug,String Review)
        {
            String received="";
            TcpClient client=null;
            NetworkStream clientStream=null;

            try
            {
            String str=Action+"~"+Drug+"~"+Review;

            Int32 Port=5050;
            client=new TcpClient(Server,Port);

            Byte[] Data=System.Text.Encoding.ASCII.GetBytes(str);
            clientStream = client.GetStream();
               
            clientStream.Write(Data,0,Data.Length);
            
            
            Data=new Byte[65536];

            Int32 Bytes=clientStream.Read(Data,0,Data.Length);
            received=System.Text.Encoding.ASCII.GetString(Data,0,Bytes);
            }
            
            catch(SocketException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("Can't connect to server. Socket error","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(IOException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("NetworkStream read or write error","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(ObjectDisposedException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("NetworkStream or TcpClient is closed","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            catch(InvalidOperationException exception)
            {
                String data=exception.Data.ToString();
                MessageBox.Show("Tcp client is not connected","Connection failed",MessageBoxButton.OK);
                Exception=true;
            }

            finally
            {
                if (clientStream!=null){
                    clientStream.Close();}
                if (client!=null){
                    client.Close();}            
            }

            return received;
        }    
    }
}
