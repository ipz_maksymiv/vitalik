﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Net;
using System.Net.Sockets;
using System.Data.OleDb;
using System.ComponentModel;



namespace DrugsServer
{
	public enum Actions
	{
		[DescriptionAttribute("IsRegistered")]
		IsRegistered,
		[DescriptionAttribute("Register")]
		Register,
		[DescriptionAttribute("GetDrugs")]
		GetDrugs,
		[DescriptionAttribute("QuantityChanged")]
		QuantityChanged,
		[DescriptionAttribute("AddToOrder")]
		AddToOrder,
		[DescriptionAttribute("GetDrugsReviews")]
		GetDrugsReviews,
		[DescriptionAttribute("GetReview")]
		GetReview,
		[DescriptionAttribute("SetReview")]
		SetReview,
		[DescriptionAttribute("OrderSum")]
		OrderSum,
		[DescriptionAttribute("GetDiscount")]
		GetDiscount,
		[DescriptionAttribute("CancelOrder")]
		CancelOrder,
		[DescriptionAttribute("UpdateClient")]
		UpdateClient
	}	

    public class Program
    {
        static void Main(string[] args)
        {
            TcpListener listener=null;
            try{
                int MaxThreads=Environment.ProcessorCount*5;
                ThreadPool.SetMaxThreads(MaxThreads,MaxThreads);
                ThreadPool.SetMinThreads(2,2);

                Int32 Port=5050;
                IPAddress Address=IPAddress.Parse("127.0.0.1");
                int counter=0;
                listener=new TcpListener(Address,Port);

                listener.Start(MaxThreads);

                for (;;){
                    Console.Write("Waiting for a connection... ");
                    ThreadPool.QueueUserWorkItem(ProcessingRequest, listener.AcceptTcpClient());
                    counter++;
                    Console.Write("\nConnection № " + counter.ToString() + "!\n");}}

            catch(SocketException exeption){
                Console.WriteLine("SocketException: {0}", exeption);}

            finally{
                listener.Stop();}
        }


        static void ProcessingRequest(object clientObj)
        {
            Byte[] Bytes=new Byte[65536];
            String data=null;

            TcpClient client=clientObj as TcpClient;

            NetworkStream stream=client.GetStream();
            

            OleDbCommand DBCommand=new OleDbCommand();
            OleDbConnection DBConnection=new OleDbConnection();
            OleDbDataReader DBReader;
                      

            DBConnection.ConnectionString="Provider=Microsoft.ACE.OLEDB.12.0;Data Source=DB.accdb";
            DBCommand.Connection=DBConnection;
           
            String query="";
            List<String> DBData=new List<String>();

            int bytes=stream.Read(Bytes,0,Bytes.Length);
            data=System.Text.Encoding.ASCII.GetString(Bytes,0,bytes);
            PersonalData pData;
            String[] strings=data.Split('~');
			Actions action=(Actions)Enum.Parse(typeof(Actions),strings[0]);
			
            try
            {
                switch(action){
					case Actions.IsRegistered:
                        query="SELECT * FROM PersonalData;";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBReader=DBCommand.ExecuteReader();
                        for (;DBReader.Read();){
                            DBData.Add(String.Join("~",DBReader[0],DBReader[1],DBReader[2]));}
                        DBReader.Close();
                        DBConnection.Close();
                        pData=new PersonalData(strings[1],strings[2],strings[3]);
                        PersonalData[] clients=new PersonalData[DBData.Count];
                        for (int a=0;a<clients.Length;a++){
                            String[] str=DBData.ElementAt(a).Split('~');
                            clients[a]=new PersonalData(str[0],str[1],str[2]);}
                    
                        data="Not";
                        for (int a=0;a<clients.Length;a++){
                            if (String.Compare(clients[a].PLast,pData.PLast,true)==0 && String.Compare(clients[a].PFirst,pData.PFirst,true)==0 && String.Compare(clients[a].PPhone,pData.PPhone,true)==0){
                                data="Authorized";
                                break;}}
                                            
                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);
                    
                        break;

                    case Actions.Register:
                        data="Unregistered";
                        query="INSERT INTO PersonalData (LastName,FirstName,Phone,Overallsum) VALUES ('"+strings[1]+"','"+strings[2]+"','"+strings[3]+"','"+"0');";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBCommand.ExecuteNonQuery();
                        DBConnection.Close();
                        data="Registered";
                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);
                        break;

                    case Actions.GetDrugs:
                        data="D~";
                        query="SELECT * FROM Drugs;";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBReader=DBCommand.ExecuteReader();
                        for (;DBReader.Read();){
                            DBData.Add(String.Join("`",DBReader[0],DBReader[1],DBReader[2]));}
                        DBReader.Close();
                        DBConnection.Close();
                   
                        for (int a=0;a<DBData.Count;a++){
                            if (String.Compare(DBData[a].Split('`')[1],"0")!=0){
                                data+=DBData.ElementAt(a)+"~";}}
                    
                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);                    
                        break;

                    case Actions.QuantityChanged:
                        query="SELECT * FROM Drugs;";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBReader=DBCommand.ExecuteReader();
                        for (;DBReader.Read();){
                            DBData.Add(String.Join("~",DBReader[0],DBReader[1],DBReader[2]));}
                        DBReader.Close();
                        DBConnection.Close();

                        data="Q~";
                        String[] Drugs=strings[1].Split('\n');
                        for (int a=0;a<Drugs.Length;a++){
                            double priceForOne=0.0;
                            if (Drugs[a]!=""){
                                for (int c=0;c<DBData.Count;c++){
                                    if (Drugs[a].Split('`')[0]==DBData.ElementAt(c).Split('~')[0]){
                                        priceForOne=Double.Parse(DBData.ElementAt(c).Split('~')[2]);
                                        break;}}
                                double tempsum=Int32.Parse(Drugs[a].Split('`')[1])*priceForOne;
                                Drugs[a]=Drugs[a].Split('`')[0]+"`"+Drugs[a].Split('`')[1].ToString()+"`"+tempsum.ToString();}}
           
                        for (int a=0;a<Drugs.Length;a++){
                            data+=Drugs[a]+"\n";}

                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);
                        break;

                    case Actions.AddToOrder:
                        query="SELECT * FROM Drugs;";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBReader=DBCommand.ExecuteReader();
                        for (;DBReader.Read();){
                            DBData.Add(String.Join("~",DBReader[0],DBReader[1]));}
                        DBReader.Close();
                       
                        String[] Added=strings[1].Split('\n');
                        for (int a=0;a<Added.Length;a++){
                            if (Added[a]!=""){
                                for (int b=0;b<DBData.Count;b++){
                                    if (Added[a].Split('`')[0]==DBData.ElementAt(b).Split('~')[0]){
                                        query="UPDATE Drugs SET Quantity='"+(Int32.Parse(DBData.ElementAt(b).Split('~')[1])-Int32.Parse(Added[a].Split('`')[1])).ToString()+"' WHERE DrugName='"+Added[a].Split('`')[0]+"';";
                                        DBCommand.CommandText=query;
                                        DBCommand.ExecuteNonQuery();
                                        break;}}}}

                        DBConnection.Close();

                        data="A~"+strings[1];
                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);
                        break;

                    case Actions.GetDrugsReviews:
                        query="SELECT * FROM Drugs;";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBReader=DBCommand.ExecuteReader();
                        for (;DBReader.Read();){
                            DBData.Add(String.Join("`",DBReader[0]));}  
                        DBReader.Close();
                        DBConnection.Close();
                   
                        data="D~";
                        for (int a=0;a<DBData.Count;a++){
                            data+=DBData.ElementAt(a)+"~";}

                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);                    
                        break;

                    case Actions.GetReview:
                        query="SELECT * FROM Drugs;";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBReader=DBCommand.ExecuteReader();
                        for (;DBReader.Read();){
                            DBData.Add(String.Join("`",DBReader[0],DBReader[3]));}  
                        DBReader.Close();
                        DBConnection.Close();
                   
                        data="G~";
                        for (int a=0;a<DBData.Count;a++){
                            String[] str=DBData.ElementAt(a).Split('`');
                            if (String.Compare(str[0],strings[1])==0){
                                data+=str[1];
                                break;}}

                        if (data.Length<3){
                            data="N";}
                    
                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);
                        break;

                    case Actions.SetReview:
                        query="SELECT * FROM Drugs;";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBReader=DBCommand.ExecuteReader();
                        for (;DBReader.Read();){
                            DBData.Add(String.Join("`",DBReader[0],DBReader[3]));}  

                        DBReader.Close();
                    
                        data="";
                        for (int a=0;a<DBData.Count;a++){
                            if (String.Compare(strings[1],DBData[a].Split('`')[0])==0){
                                data+=DBData.ElementAt(a).Split('`')[1]+"~";
                                break;}}
                        if (data[0]=='~'){
                            data="";}
                        data+=strings[2];

                        query="UPDATE Drugs SET Reviews='"+data+"' WHERE DrugName='"+strings[1]+"';";
                        DBCommand.CommandText=query;
                        DBCommand.ExecuteNonQuery();
                        DBConnection.Close();

                        data="S";
                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);
                        break;

                    case Actions.OrderSum:
                        double sum=0.0;
                        query="SELECT * FROM Drugs;";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBReader=DBCommand.ExecuteReader();
                        for (;DBReader.Read();){
                            DBData.Add(String.Join("`",DBReader[0],DBReader[1],DBReader[2]));}  
                        DBReader.Close();
                        DBConnection.Close();
                   
                        data="S~";
                        String[] InOrderAll=strings[4].Split('\n');
                        
                        for (int a=0;a<InOrderAll.Length;a++){
                            if (InOrderAll[a]!="")
                            sum+=Double.Parse(InOrderAll[a].Split('`')[2]);}

                        data+=sum.ToString();

                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);                    
                        break;

                    case Actions.GetDiscount:
                        pData=new PersonalData(strings[1],strings[2],strings[3]);
                        query="SELECT * FROM PersonalData;";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBReader=DBCommand.ExecuteReader();
                        for (;DBReader.Read();){
                            DBData.Add(String.Join("~",DBReader[0],DBReader[1],DBReader[2],DBReader[3]));}  
                        DBReader.Close();
                        DBConnection.Close();
                   
                        data="D~";
                        PersonalData[] allclients=new PersonalData[DBData.Count];
                        for (int a=0;a<allclients.Length;a++){
                            String[] str=DBData.ElementAt(a).Split('~');
                            allclients[a]=new PersonalData(str[0],str[1],str[2],Double.Parse(str[3]));}
                    
                        for (int a=0;a<allclients.Length;a++){
                            if (String.Compare(allclients[a].PLast,pData.PLast,true)==0 && String.Compare(allclients[a].PFirst,pData.PFirst,true)==0 && String.Compare(allclients[a].PPhone,pData.PPhone,true)==0){
                                if (allclients[a].POverall<1000){
                                    data+="0.0~"+strings[5];}
                                else if (allclients[a].POverall>=1000 && allclients[a].POverall<2000){
                                    double discount=Double.Parse(strings[5])*0.05;
                                    data+=discount.ToString()+"~"+(Double.Parse(strings[5])-discount).ToString();}
                                else if (allclients[a].POverall>=2000 && allclients[a].POverall<3500){
                                    double discount=Double.Parse(strings[5])*0.1;
                                    data+=discount.ToString()+"~"+(Double.Parse(strings[5])-discount).ToString();}
                                else if (allclients[a].POverall>=3500 && allclients[a].POverall<5000){
                                    double discount=Double.Parse(strings[5])*0.15;
                                    data+=discount.ToString()+"~"+(Double.Parse(strings[5])-discount).ToString();}
                                else{
                                    double discount=Double.Parse(strings[5])*0.2;
                                    data+=discount.ToString()+"~"+(Double.Parse(strings[5])-discount).ToString();}
                                break;}}

                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);                    
                        break;

                    case Actions.CancelOrder:
                        query="SELECT * FROM Drugs;";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBReader=DBCommand.ExecuteReader();
                        for (;DBReader.Read();){
                            DBData.Add(String.Join("~",DBReader[0],DBReader[1]));}
                        DBReader.Close();
                       
                        String[] Canceled=strings[4].Split('\n');
                        for (int a=0;a<Canceled.Length;a++){
                            if (Canceled[a]!=""){
                                for (int b=0;b<DBData.Count;b++){
                                    if (Canceled[a].Split('`')[0]==DBData.ElementAt(b).Split('~')[0]){
                                        query="UPDATE Drugs SET Quantity='"+(Int32.Parse(DBData.ElementAt(b).Split('~')[1])+Int32.Parse(Canceled[a].Split('`')[1])).ToString()+"' WHERE DrugName='"+Canceled[a].Split('`')[0]+"';";
                                        DBCommand.CommandText=query;
                                        DBCommand.ExecuteNonQuery();
                                        break;}}}}

                        DBConnection.Close();

                        data="C";
                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);
                        break;

                    case Actions.UpdateClient:
                        query="SELECT * FROM PersonalData;";
                        DBCommand.CommandText=query;
                        DBConnection.Open();
                        DBReader=DBCommand.ExecuteReader();
                        for (;DBReader.Read();){
                            DBData.Add(String.Join("`",DBReader[0],DBReader[1],DBReader[2],DBReader[3]));}  

                        DBReader.Close();
                    
                        data="";
                        for (int a=0;a<DBData.Count;a++){
                            if (String.Compare(strings[1],DBData[a].Split('`')[0])==0 && String.Compare(strings[2],DBData[a].Split('`')[1])==0 && String.Compare(strings[3],DBData[a].Split('`')[2])==0){
                                double newoverall=Double.Parse(DBData[a].Split('`')[3])+Double.Parse(strings[5]);
                                query="UPDATE PersonalData SET Overallsum='"+newoverall.ToString()+"' WHERE LastName='"+strings[1]+"' AND FirstName='"+strings[2]+"' AND Phone='"+strings[3]+"';";
                                DBCommand.CommandText=query;
                                DBCommand.ExecuteNonQuery();
                                break;}}
                                        
                        DBConnection.Close();

                        data="U";
                        stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);
                        break;}
            }

            catch(OleDbException exception)
            {
                data=exception.Data.ToString();
                data="E~Can't find data base or bad data.~Data base error.";
                Console.WriteLine("\nCan't find data base or bad data!");
                stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);
            }

            catch(InvalidOperationException exception)
            {
                data=exception.Data.ToString();
                data="E~Error in data base access.~Data base error.";
                Console.WriteLine("\nError in data base access!");
                stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);
            }

            catch(IndexOutOfRangeException exception)
            {
                data=exception.Data.ToString();
                data="E~Error in data base columns quantity.~Data base error.";
                Console.WriteLine("\nError in data base columns quantity!");
                stream.Write(System.Text.Encoding.ASCII.GetBytes(data),0,System.Text.Encoding.ASCII.GetBytes(data).Length);
            }


            finally
            {
                client.Close();
            }
        }      
    }
}
