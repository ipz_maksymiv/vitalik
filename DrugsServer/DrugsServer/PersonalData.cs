﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DrugsServer
{
    public class PersonalData
    {
        private String LastName;
        private String FirstName;
        private String Phone;
        private double Overall;

        public PersonalData()
        {
            LastName="";
            FirstName="";
            Phone="";
            Overall=0.0;
        }

        public PersonalData(String last,String first,String phone,double overall=0.0)
        {
            LastName=last;
            FirstName=first;
            Phone=phone;
            Overall=overall;
        }

        ~PersonalData(){}

        public String PLast
        {
            get{return LastName;}
            set{LastName=value;}
        }

        public String PFirst
        {
            get{return FirstName;}
            set{FirstName=value;}
        }

        public String PPhone
        {
            get{return Phone;}
            set{Phone=value;}
        }

        public double POverall
        {
            get{return Overall;}
            set{Overall=value;}
        }
    };
}
